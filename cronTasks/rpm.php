<?php
// rpm.php - (c) RedWarn Contributors - Apache License 2.0
// Core by Ed. E
// Works out the reverts per min. on English Wikipedia between the 50 latest undos and 50 rollbacks (100 revs total)
/*
WARNING! This file is run on a cron-job and MUST NOT be public due to DOS concerns.
*/

// get reverts made via mw-undo
$undoReverts = json_decode(file_get_contents('https://en.wikipedia.org/w/api.php?action=query&list=recentchanges&rctag=mw-undo&rclimit=50&rcprop=timestamp&format=json'))->query->recentchanges;
// get reverts made via mw-rollback
$rollbackReverts = json_decode(file_get_contents('https://en.wikipedia.org/w/api.php?action=query&list=recentchanges&rctag=mw-rollback&rclimit=50&rcprop=timestamp&format=json'))->query->recentchanges;

// Now get timestamps 
$undoRevertLatest = strtotime($undoReverts[0]->timestamp);
$undoRevertOldest = strtotime($undoReverts[49]->timestamp);

$rollbackRevertLatest = strtotime($rollbackReverts[0]->timestamp);
$rollbackRevertOldest = strtotime($rollbackReverts[49]->timestamp);

// Now calculate RPM - 50 (count of reverts) / time difference in seconds = reverts per second * 60 = rpm
$undoRPM = round((50 / ($undoRevertLatest - $undoRevertOldest)) * 60, 2);
$rollbackRPM = round((50 / ($rollbackRevertLatest - $rollbackRevertOldest)) * 60, 2);
$totalRPM = $undoRPM + $rollbackRPM;

// Now append to file. RM if first value is >24 hours ago
echo '
Current time: '. time() .'
Undo RPM: '.$undoRPM.'
Rollback RPM: '.$rollbackRPM.'
Total RPM: '.$totalRPM;

// Read file
// Format: timestamp|undoRPM|rollbackRPM|totalRPM
$currentLogContents = file_get_contents("/mnt/nfs/labstore-secondary-tools-project/redwarn/cronTools/rpmData.txt");
$currentLogData = explode("\n", $currentLogContents);
// If this record is > 24hrs old
if (((int)explode("|", $currentLogData[0])[0] <= strtotime('-24 hours'))) {
    // Remove it
    array_shift($currentLogData);
}
// Now add our data split by |
$currentLogData[] = implode("|", [time(), $undoRPM, $rollbackRPM, $totalRPM]);

// Rebuild and write
file_put_contents("/mnt/nfs/labstore-secondary-tools-project/redwarn/cronTools/rpmData.txt", implode("\n", $currentLogData));

// Done!
echo '
Log saved.
';